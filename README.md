# **Bespoken End-To-End Testing - Comprehensive Automation Solution**
This sample testing project is a boilerplate to start creating functional test scripts. It leverages Bespoken, DataDog, and GitLab CI.

The combination of these tools provides in-depth testing, routine builds, tracking of historical results, and graphical reporting and analytics.

It's a powerful combnination!

## **Getting Started**
### **Getting Started With Gitlab**
We recommend running tests inside of Gitlab. It makes it easy to:
* Run tests in a clean environment
* Run tests on a regular schedule
* Share your tests with other team members

To get started using these tests in Gitlab, you can just select 'Fork' on the top-right and follow the directions.  
(If you have not already signed up for a Gitlab account, you will need to do this first)

It will duplicate this project, in your own Gitlab environment!

Once forked, you can drop-in your own tests in the `e2e` folder to being using it to test your own voice experiences.

### **Getting Started Locally**
Alternatively, you can run the tests within your own machine (or in a different hosted environment).

Get your local copy of this project by cloning it from this repository.
```bash
git clone https://gitlab.com/bespoken/e2e-tester.git
```

To make it work, you have to install the project dependencies from the project root:
```bash
npm install
```

### **Virtual Device Setup**
We use Bespoken Virtual Devices to transform the text test scripts into audio, send them to the voice assistant and process the responses. A virtual device works like a physical device, such as Amazon Echo, but can be interacted with via our [simple test scripting syntax](https://read.bespoken.io/end-to-end/guide.html) (as well as programmatically via our [API](https://read.bespoken.io/end-to-end/api.html)).

- Create a virtual device with our easy-to-follow guide [here](https://read.bespoken.io/end-to-end/setup/#creating-a-virtual-device).
- Add it to the configuration file (`testing.json`), as described below
  ```json
  {  
    "virtualDeviceToken": {
      "alexa": {
        "en-US": "<YOUR_VIRTUAL_DEVICE>"
      }
    }
  }
  ```
### **Environment Variables**
There are a couple of environment variables used during test execution:
- `DATADOG_API_KEY`: As we send the test execution results to DataDog for reporting it is necessary to create a DataDog account and then generate an API key to receive the test data.
- `JEST_STARE_INLINE_SOURCE`: When setting to true, it generates a one-file HTML test report containing the test results.

To add these in Gitlab, go to `Settings -> CI / CD -> Variables` and add the environment variables, with the appropriatve values, there.

For running locally, both values can be set either in a `.env` file (you can rename the `example.env` and include your values on it); or set them temporarily from the command line with the `export` command:
```bash
export JEST_STARE_INLINE_SOURCE=true
```
(Use `set` instead of `export` if using Windows Command Prompt).

## **Running Tests**
### **Running Tests In Gitlab**
To manually start a test run in Gitlab, do this: 
* Go to `CI / CD -> Pipelines`
* Click `Run Pipeline`
* Click `Run Pipeline` again

Easy, right?

If using this project as a Fork from Gitlab, the testing are controlled via the file `.gitlab-ci.yml`:
```yaml
image: node:10

cache:
  paths:
  - node_modules/

utterance-tests:
  script:
   - npm install
   - npm test
  artifacts:
    when: always
    paths:
      - test_output/report/index.inline.html
      - test-report.xml
    reports:
      junit: test-report.xml
  only: 
    - schedules
    - web
```

When the GitLab Runner is executed, it takes this file and creates a Linux instance with Node.js, executes the commands under the `script` element, and saves the reports as artifacts.

#### **Setting a schedule**
It is very easy to run your end-to-end tests regularly using GitLab. Once your CI file (`.gitlab-ci.yml`) has been uploaded to the repository just go to "CI/CD => Schedules" from the left menu. Create a new schedule, it looks like this:

[<img src="docs/images/GitLabCISchedule-1.png" width="50%">](docs/images/GitLabCISchedule-1.png)

[<img src="docs/images/GitLabCISchedule-2.png" width="50%">](docs/images/GitLabCISchedule-2.png)

### **Running Tests Locally**
Once installed the dependencies and added your Virtual Device to the `testing.json` file, execute the sample test scripts (located under `e2e` folder) with:
```bash
npm test
```

If you prefer to execute only one test suite, you can do it like this:
```bash
npm test e2e\invokingGame.e2e.yml
```

When finished, the test results look like this:

[<img src="docs/images/LocalTestResults.png" width="50%">](docs/images/LocalTestResults.png)

A single-file report (`index.inline.html`) is also generated under `test_output\report`.

[<img src="docs/images/InlineTestReport.png" width="50%">](docs/images/InlineTestReport.png)

## **Test Results**
### **Gitlab Test Results**
You can also see the test results if the execution was completed on GitLab CI. To check the output just open the test tab of any completed pipeline:

[<img src="docs/images/GitLabCITestResults-1.png" width="50%">](GitLabCITestResults-1.png)

Then, click on any of the executed test suites:

[<img src="docs/images/GitLabCITestResults-2.png" width="50%">](GitLabCITestResults-2.png)

All the executed tests, and their results will be displayed:

[<img src="docs/images/GitLabCITestResults-3.png" width="50%">](GitLabCITestResults-3.png)

### **Local Reporting**
You can find the inline HTML report containing the test results in the `test_output\report` folder. To check the report just open the file named `index.inline.html` with your browser. It looks like this:

[<img src="docs/images/InlineTestReport.png" width="50%">](docs/images/InlineTestReport.png)

## **DataDog**
DataDog captures metrics related to how all the tests have performed. Each time we run the tests, we push the result of each test to DataDog.

We are using these metrics:
- `utterance.success`
- `utterance.failure`
- `test.success`
- `test.failure`

The metrics can be easily reported on through a DataDog Dashboard. They also can be used to set up notifications when certain conditions are triggered.

### **How to signup and get API key**
[DataDog](https://www.datadoghq.com/) has several plans for different use cases. You can start a 14-day free trial and get your API Key to collect the test results on your dashboard, or use ours, included in our packaged solutions (for more information, [contact us](mailto:sales@bespoken.io)).

After the signup process, you can create your API key following these steps:
1. Select `APIs` from the left `Integrations` menu.
2. Create a new API key in the `API Keys` section.

Then you can copy the value to your `.env` file, or add it as an environment variable on your CI settings.

### **How to export and import a Dashboard**
A DataDog dashboard is a tool for visually tracking, analyzing, and displaying key performance metrics, which enable you to monitor the health of your voice apps. You can create a Dashboard by using the `utterance` and `test` metrics. To help you get started you can import our sample Dashboard by following the next steps:
1. Create a dashboard by selecting `New Dashboard` from the left `Dashboards` menu.
2. Give your dashboard a name, and then click on the `New Timeboard`.
3. Select `Import Dashboard JSON` from the settings cog (upper right corner)
4. Drag [this file](https://gitlab.com/bespoken/e2e-tester/-/blob/master/docs/BespokenDataDogDashboard.json) into the `Import dashboard JSON` modal window
5. Click on `Yes, Replace`

Now, whenever you run your test scripts, the test results will be automatically sent and stored in DataDog.

[<img src="docs/images/DataDogDashboard.png" width="50%">](docs/images/DataDogDashboard.png)